#!/bin/bash
# ensure LOCAL bash
if ! [ -n "$BASH_VERSION" ];then
    echo "this is not bash, calling self with bash....";
    SCRIPT=$(readlink -f "$0")
    /bin/bash $SCRIPT
    exit;
fi


NAMESPACE=""
REPOSITORY=""
TAG=""

while getopts n:r:t:hd flag
do
    case "${flag}" in
        n) NAMESPACE=${OPTARG};;
        r) REPOSITORY=${OPTARG};;
        t) TAG=${OPTARG};;
        h) echo "-n NAMESPACE -r REPOSITORY -t TAG" >&2
                        exit 0;;
        *) echo 'Unknown parameter' >&2
                        exit 1;;
    esac
done

if [ -z "$NAMESPACE" ] || [ -z "$REPOSITORY" ] || [ -z "$TAG" ]; then
    echo "-n NAMESPACE -r REPOSITORY -t TAG are all required" >&2
    exit 1
 fi


###############################################
# Using hubtool from here: https://github.com/docker/hub-tool/

if [ ! -f hub-tool/hub-tool ]; then
    echo "Downloading hub-tool..."
    wget -c https://github.com/docker/hub-tool/releases/download/v0.4.4/hub-tool-linux-amd64.tar.gz -O - | tar -xz
fi

############################################

LOCAL=`docker inspect --format='{{.RepoDigests}}' "$NAMESPACE/$REPOSITORY:$TAG" | cut -d '@' -f2 | sed "s/]//g"`

############################################

# Hub-tool defaults to sorting by last-updated if multiple values are returned, so use the first result

LATEST_FULL=`./hub-tool/hub-tool tag ls $NAMESPACE/$REPOSITORY | grep $TAG `
REG="sha256:[0-9a-z]+"
if [[ $LATEST_FULL =~ $REG ]]; then
  LATEST=${BASH_REMATCH[0]}
  UPDATED=`echo $LATEST_FULL | sed -e 's/\(.\+\)\(active\s\+\)\(.\+ago\)\(.\+\)/\3/g' `
fi


############################################

echo "$NAMESPACE/$REPOSITORY:$TAG"

if [ "$LOCAL" == "$LATEST" ];then
    echo "No update found."
else
    echo "Update found!"
    echo "Local:  $LOCAL"
    echo "Latest: $LATEST"
    echo "Updated $UPDATED"

fi
