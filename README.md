# Git Image Update Checker

Quick tool for checking if a local docker image is up-to-date with the registry, or if it needs an update

Makes use of https://github.com/docker/hub-tool (experimental CLI for Docker Hub). 

Current limitations: need to login with Hub-tool for some functionality

## Usage

        - n NAMESPACE
        - r REPOSITORY
        - t TAG
  e.g. to check `katettudelft/noetic_dev:v3` for updates:

  ```
  ./checkdockernewertag.sh -t v3 -n katettudelft -r foxy-dev
  ```

 ## Sample Output: 
  
  identical versions found:
```
katettudelft/noetic_dev:v2
No update needed.
```

image exists on registry, but not local:
```
Error: No such object: katettudelft/foxy-dev:v1
katettudelft/foxy-dev:v1
update found!
Local:  
Latest: sha256:a73a712a7e14049c27162627a1a3637df5b20e6f3d8c69074c9e7e8776b84073
```